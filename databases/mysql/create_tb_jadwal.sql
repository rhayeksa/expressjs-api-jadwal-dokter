-- db_dokter.tb_jadwal definition

CREATE TABLE `tb_jadwal` (
  `jadwal_id` int NOT NULL AUTO_INCREMENT,
  `days` varchar(15) NOT NULL,
  `time_start` varchar(5) NOT NULL,
  `time_finish` varchar(5) NOT NULL,
  `quota` int NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dates` date NOT NULL,
  `dokter_id` int NOT NULL,
  PRIMARY KEY (`jadwal_id`),
  KEY `tb_jadwal_tb_dokter_FK` (`dokter_id`),
  CONSTRAINT `tb_jadwal_tb_dokter_FK` FOREIGN KEY (`dokter_id`) REFERENCES `tb_dokter` (`dokter_id`)
);
