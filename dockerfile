FROM node:18.20-slim

WORKDIR /code
COPY . .

RUN npm install -g npm@10.5.1
RUN npm install
RUN npm run swagger-autogen

CMD [ "npm", "start" ]

EXPOSE 8000