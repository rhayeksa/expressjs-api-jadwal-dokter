// library
const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger-output.json");

// import router
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const dokterRouter = require("./routes/dokter");

// define app
const app = express();

// config app
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// routes
app.use("/", indexRouter);
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use("/users", usersRouter);
app.use("/dokter", dokterRouter);

module.exports = app;
