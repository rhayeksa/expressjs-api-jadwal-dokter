const { Router } = require("express");
const { getToken } = require("../controllers/users/getToken");
const router = Router();

router.get("/get-token", getToken);

module.exports = router;
