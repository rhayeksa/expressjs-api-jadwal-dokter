const { Router } = require("express");
const { response } = require("../utils/response");
const router = Router();

router.get("/", (req, res) => {
  return response({ req, res, code: 200, msg: "Rest API Jadwal Dokter" });
});

module.exports = router;
