const { Router } = require("express");
const router = Router();
const {
  getAllJadwalDokter,
} = require("../controllers/dokter/getAllJadwalDokter");
const {
  insertJadwalDokter,
} = require("../controllers/dokter/insertJadwalDokter");
const { verifyToken } = require("../middlewares/verifyToken");

router.get("/get-all-jadwal-dokter", [verifyToken], getAllJadwalDokter);
// router.post("/insert-jadwal-dokter", [verifyToken], insertJadwalDokter);
router.post(
  "/insert-jadwal-dokter",
  [verifyToken],
  insertJadwalDokter(req, res, asdf)
);
router.get("", verifyToken, getAllJadwalDokter);

module.exports = router;
