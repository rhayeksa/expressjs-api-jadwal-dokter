const { sign } = require("jsonwebtoken");
const { response } = require("../../utils/response");
const { secret } = require("../../configs/secret");

exports.getToken = async (req, res) => {
  // #swagger.tags = ['Users']
  return response({
    req,
    res,
    code: 200,
    msg: "Token berhasil di generate",
    data: {
      accessToken: sign({ userId: 1 }, secret),
    },
  });
};
