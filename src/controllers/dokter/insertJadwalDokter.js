require("dotenv/config");
const { sequelize, Sequelize } = require("../../configs/db");
const { response } = require("../../utils/response");
const datesBetween = require("dates-between");
const db_name = process.env.DB_NAME;

exports.insertJadwalDokter = async (req, res) => {
  // #swagger.tags = ['Dokter']
  const t = await sequelize.transaction();
  try {
    let {
      doctorName,
      day,
      timeStart,
      timeFinish,
      quota,
      status,
      dateRangeFrom,
      dateRangeTo,
    } = req.body;
    let data;

    // get dokter_id dan validasi doctor name
    data = await sequelize.query(
      `
      SELECT dokter_id
      FROM ${db_name}.tb_dokter
      WHERE nama = $name
      `,
      {
        type: Sequelize.QueryTypes.SELECT,
        transaction: t,
        bind: { name: doctorName },
      }
    );
    if (data.length < 1) {
      return await response({
        req,
        res,
        code: 404,
        msg: "Dokter tidak ditemukan",
      });
    } else if (data.length > 1) {
      return await response({
        req,
        res,
        code: 409,
        msg: "Nama Dokter duplikat",
      });
    }
    const dokterId = data[0]["dokter_id"];

    // time validasi
    if (
      !String(timeStart).match(/^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/) ||
      !String(timeFinish).match(/^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/)
    ) {
      return await response({
        req,
        res,
        code: 400,
        msg: 'Time start atau time finish tidak valid, contoh time yang valid "23:59"',
      });
    }

    // quota validasi
    if (!Number(quota)) {
      return await response({
        req,
        res,
        code: 400,
        msg: "Quota tidak valid, input dengan angka",
      });
    }

    // staus validasi
    if (![true, 1, false, 0].includes(status)) {
      return await response({
        req,
        res,
        code: 400,
        msg: "Status tidak valid, input dengan true, 1, false, atau 0",
      });
    }

    // date validasi
    if (
      !String(req.body.dateRangeFrom).match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/) ||
      !String(req.body.dateRangeTo).match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/)
    ) {
      return await response({
        req,
        res,
        code: 400,
        msg: 'Date range from atau to tidak valid, contoh date range yang valid "2024-12-31"',
      });
    }

    // day validasi
    const dayNumber = [
      "sunday",
      "monday",
      "tuesday",
      "wednesday",
      "thursday",
      "friday",
      "saturday",
    ].indexOf(String(day).toLowerCase());
    if (dayNumber < 0) {
      return await response({ req, res, code: 400, msg: "Hari tidak valid" });
    }
    // get date by day
    let dateArr = [];
    for (const date of datesBetween(
      new Date(dateRangeFrom),
      new Date(dateRangeTo)
    )) {
      if (date.getDay() === dayNumber /*Sunday = 0 ... Saturday = 6*/) {
        dateArr.push(date.toLocaleDateString("en-CA"));
      }
    }

    // Insert data
    for (let i = 0; i < dateArr.length; i++) {
      data = await sequelize.query(
        `
        INSERT INTO ${db_name}.tb_jadwal(days, time_start, time_finish, quota, status, dates, dokter_id)
        VALUES ($day, $time_start, $time_finish, $quota, $status, $date, $doctor_id)
        `,
        {
          type: Sequelize.QueryTypes.INSERT,
          transaction: t,
          bind: {
            day: day,
            time_start: timeStart,
            time_finish: timeFinish,
            quota: quota,
            status: req.body.status,
            date: dateArr[i],
            doctor_id: dokterId,
          },
        }
      );
    }
    t.commit();
    return await response({
      req,
      res,
      code: 200,
      msg: "Berhasil insert jadwal Dokter",
    });
  } catch (error) {
    t.rollback();
    console.error(`\n\nError : ${error}\n\n`);
    return await response({ req, res, code: 500 });
  }
};
