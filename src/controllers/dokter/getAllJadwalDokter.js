require("dotenv/config");
const { sequelize, Sequelize } = require("../../configs/db");
const { pagination } = require("../../utils/pagination");
const { response } = require("../../utils/response");
const db_name = process.env.DB_NAME;

exports.getAllJadwalDokter = async (req, res) => {
  // #swagger.tags = ['Dokter']
  let data, qry;
  let size = !req.query.size ? 5 : req.query.size;
  let page = !req.query.page ? 1 : req.query.page;
  const t = await sequelize.transaction();
  try {
    qry = await sequelize.query(
      `
      SELECT
        tj.jadwal_id AS id
        , tj.dokter_id
        , tj.days AS day
        , tj.time_start
        , tj.time_finish
        , tj.quota
        , IF(tj.status = 1, "true", "false") as status
        , td.nama AS doctor_name
        , DATE_FORMAT(tj.dates, '%Y-%m-%d') AS date
      FROM ${db_name}.tb_jadwal tj 
      INNER JOIN ${db_name}.tb_dokter td ON td.dokter_id = tj.dokter_id
      ORDER BY tj.jadwal_id DESC
      LIMIT $limit OFFSET $offset
      `,
      {
        type: Sequelize.QueryTypes.SELECT,
        transaction: t,
        bind: { limit: String(size), offset: String((page - 1) * size) },
      }
    );
    data = qry;

    qry = await sequelize.query(
      `
      SELECT
        COUNT(1) AS totalData 
      FROM ${db_name}.tb_jadwal tj 
      INNER JOIN ${db_name}.tb_dokter td ON td.dokter_id = tj.dokter_id      
      `,
      {
        type: Sequelize.QueryTypes.SELECT,
        transaction: t,
      }
    );

    t.commit();
    return await response({
      req,
      res,
      code: 200,
      msg: "Berhasil",
      page: await pagination({
        pageSize: Number(size),
        currentPage: Number(page),
        totalData: qry[0]["totalData"],
      }),
      data: data,
    });
  } catch (error) {
    t.rollback();
    console.error(`\n\nError : ${error}\n\n`);
    return await response({ req, res, code: 500 });
  }
};
